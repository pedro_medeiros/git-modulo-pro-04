package Filmes.lista.Repository;

import Filmes.lista.Filmes;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class FilmesRepository {

    private final List<Filmes> filme;

    public FilmesRepository(){
        this.filme = new ArrayList<>();
    }

    public List<Filmes> findAll (){
        return filme;
    }

    public List<Filmes> findAll(final Filmes filmes) {
        if (filmes.getNome() != null) {
            return filme.stream()
                    .filter(flm -> flm.getNome().contains(filmes.getNome()))
                    .collect(Collectors.toList());
        }
        if (filmes.getDiretor() != null) {
            return filme.stream()
                    .filter(flm -> flm.getDiretor().contains(filmes.getDiretor()))
                    .collect(Collectors.toList());
        }
        if (filmes.getId() != null) {
            return filme.stream()
                    .filter(flm -> flm.getId().equals(filmes.getId()))
                    .collect(Collectors.toList());
        }
        if (filmes.getAno() != null) {
            return filme.stream()
                    .filter(flm -> flm.getAno().equals(filmes.getAno()))
                    .collect(Collectors.toList());
        }
        if (filmes.getNota() != null) {
            return filme.stream()
                    .filter(flm -> flm.getNota().equals(filmes.getNota()))
                    .collect(Collectors.toList());
        }
        return findAll();
    }

    public void update(final Filmes filmes){
        filme.stream()
                .filter(flm -> flm.getId().equals(filmes.getId()))
                .forEach(flm -> flm.setNome(filmes.getNome()));
        filme.stream()
                .filter(flm -> flm.getId().equals(filmes.getId()))
                .forEach(flm -> flm.setDiretor(filmes.getDiretor()));
    }

    public void delete (@PathVariable("id") Integer id){
        filme.removeIf(flm -> flm.getId().equals(id));
    }

    public void add(Filmes filmes){
        this.filme.add(filmes);
    }

    public int count(){
        return filme.size();
    }
}
