package Filmes.lista.Controller;

import Filmes.lista.Filmes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/Filmes")
public class FilmesController {
    private final List<Filmes> filme;

    @Autowired
    private Filmes.lista.Service.FilmesService FilmesService;

    public FilmesController() {
        this.filme = new ArrayList<>();
    }
    @GetMapping
    public List<Filmes> findAll(@RequestParam(required = false) Filmes filmes) {

        return FilmesService.findAll(filmes);

    }
    @PostMapping
    public ResponseEntity<Integer> add(@RequestBody final Filmes filmes) {
        Integer id = FilmesService.add(filmes);
        return new ResponseEntity<>(filmes.getId(), HttpStatus.CREATED);

    }
    @PutMapping
    public ResponseEntity update(@RequestBody final Filmes filmes) {
        FilmesService.update(filmes);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable("id") Integer id) {
        FilmesService.delete(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

}
