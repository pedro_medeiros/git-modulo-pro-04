package Filmes.lista.Service;

import Filmes.lista.Filmes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import java.util.ArrayList;
import java.util.List;


public class FilmesService {

    private final List<Filmes> filme;

    @Autowired
    private Filmes.lista.Repository.FilmesRepository FilmesRepository;

    public FilmesService() {
        this.filme = new ArrayList<>();
    }

    public List<Filmes> findAll (Filmes filmes){
        if (filmes.getNome() != null) {
            return FilmesRepository.findAll(filmes);
        }
        if (filmes.getDiretor() != null) {
            return FilmesRepository.findAll(filmes);
        }
        if (filmes.getId() != null) {
            return FilmesRepository.findAll(filmes);
        }
        if (filmes.getAno() != null){
            return FilmesRepository.findAll(filmes);
        }
        if (filmes.getNota() != null){
            return FilmesRepository.findAll(filmes);
        }
        return FilmesRepository.findAll();
    }

    public Integer add (Filmes filmes){
        if (filmes.getId() == null) {
            filmes.setId(FilmesRepository.count() + 1);
        }
        FilmesRepository.add(filmes);
        return filmes.getId();
    }
    public void update ( final Filmes filmes){
        FilmesRepository.update(filmes);

    }
    public void delete (@PathVariable("id") Integer id){
       FilmesRepository.delete(id);
    }

}
